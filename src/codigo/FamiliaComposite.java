package codigo;

import java.util.ArrayList;
import java.util.List;

public class FamiliaComposite extends FamiliaComponente{
	
	List<FamiliaComponente> filhos = new ArrayList<>();
	String tabulacao = "";	

	public FamiliaComposite(String nome) {		
		super(nome);		
	}	
	
	@Override
	public void Exibir(String tabulacao) {		
		
		System.out.printf("\n" + tabulacao + "> Familia: %s ", nome);		
		tabulacao = tabulacao + ("----------");		
		
		for (FamiliaComponente componente: filhos) {
			componente.Exibir(tabulacao);		
		}
	}
	
	@Override
	public void add(FamiliaComponente componente){
	       this.filhos.add(componente);
	}
	
	@Override
	public void rem(FamiliaComponente filho) {
		this.filhos.remove(filho);
		System.out.println("Filho: " + filho.nome + " removido dos Pais: " + this.nome);		
	}
	
	@Override
	public boolean temFilhos() {
		return !filhos.isEmpty();
	};

	public List<FamiliaComponente> getFilhos() {
		return filhos;
	}

	public void setFilhos(List<FamiliaComponente> filhos) {
		this.filhos = filhos;
	}
	
}
