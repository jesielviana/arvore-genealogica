package codigo;

import java.io.IOException;

public interface Exportador {
    public String NOME_ARQUIVO = "arvore-genealogica";

    void exportar(FamiliaComponente componente);
}
