package codigo;

import com.google.gson.Gson;

import java.io.FileWriter;
import java.io.IOException;

public class AdaptadorExportadorJSON implements Exportador {
    @Override
    public void exportar(FamiliaComponente componente) {
        Gson gson = new Gson();
        try (FileWriter writer = new FileWriter(NOME_ARQUIVO+".json")) {
            gson.toJson(componente, writer);
            System.out.println("Registro exportado: " + NOME_ARQUIVO+".json");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
