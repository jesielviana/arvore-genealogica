package codigo;

public class Filhos extends FamiliaComponente{

	public Filhos(String nome) {
		super(nome);
	}

	@Override
	public void Exibir(String tabulacao) {
		System.out.printf("\n" + tabulacao + " * Filho: %s ", nome);		
	}
	
	@Override
	public void add(FamiliaComponente componente){
	   System.out.println("Elemento filho n�o pode receber outros elementos!");
	}
	
	@Override
	public void rem(FamiliaComponente filho) {		
		System.out.println("Elemento filho n�o possui outros elementos para serem removidos!");		
	}
	
	@Override
	public boolean temFilhos() {
		return false; // elemento filho n�o tem filhos
	};
		
}
