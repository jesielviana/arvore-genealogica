package codigo;

public abstract class FamiliaComponente {

	protected  String nome;
	
	public FamiliaComponente(String nome) {
		this.nome = nome;
    }
	
    public abstract void Exibir(String tabulacao);
    
    public abstract void add(FamiliaComponente componente);
    public abstract void rem(FamiliaComponente componente);
    public abstract boolean temFilhos();

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FamiliaComponente other = (FamiliaComponente) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}
    
    
}
