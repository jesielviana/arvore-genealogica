package codigo;

import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import java.io.FileWriter;
import java.io.IOException;

public class AdadpadorExportadorYAML implements Exportador {
    @Override
    public void exportar(FamiliaComponente componente) {
        try {
            DumperOptions options = new DumperOptions();
            options.setIndent(2);
            options.setPrettyFlow(true);
            options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
            Yaml yaml = new Yaml(options);
            FileWriter writer = new FileWriter(NOME_ARQUIVO+".yaml");
            yaml.dump(componente, writer);
            System.out.println("Registro exportado: " + NOME_ARQUIVO+".yaml");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
