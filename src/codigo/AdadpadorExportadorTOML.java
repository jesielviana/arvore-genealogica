package codigo;


import com.moandjiezana.toml.TomlWriter;

import java.io.File;
import java.io.IOException;

public class AdadpadorExportadorTOML implements Exportador {
    @Override
    public void exportar(FamiliaComponente componente) {
        try {
            TomlWriter tomlWriter = new TomlWriter.Builder()
                    .indentValuesBy(2)
                    .indentTablesBy(4)
                    .padArrayDelimitersBy(3)
                    .build();
            tomlWriter.write(componente, new File(NOME_ARQUIVO+".toml"));
            System.out.println("Registro exportado: " + NOME_ARQUIVO+".toml");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
