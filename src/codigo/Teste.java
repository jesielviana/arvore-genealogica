package codigo;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Teste {
	
	public static void imprimirMenu() {		 
		 System.out.println("**********************************");
		 System.out.println("          Menu de op��es          ");
		 System.out.println("**********************************");
		 System.out.println(" 0 - Mostrar Menu                 ");
		 System.out.println(" 1 - Mostrar �rvore Atual         ");
		 System.out.println(" 2 - Add Familia                  ");
		 System.out.println(" 3 - Add Filho                    ");
		 System.out.println(" 4 - Add Familia Em Outra Fam�lia ");
		 System.out.println(" 5 - Remover elemento             ");
		 System.out.println(" 6 - Exportar Lista               ");
		 System.out.println(" 7 - Sair                         ");
		 System.out.println("**********************************");	
	 }	 
	 
	 public static void fecharPrograma(Scanner sc) {
		 System.out.println("At� mais.");
		 sc.close();
		 System.exit(0);	
	 }
	 
	 public static void mostrarArvoreAtual(FamiliaComponente familia) {
		 if (familia != null) {
			 	System.out.println("********* �rvore Geneal�gica *********");
				familia.Exibir("");
				System.out.println("\n\n**************************************");
			}else {
				System.out.println("Nenhum objeto na �rvore!");
			} 
	 }
	 
	 public static FamiliaComponente procurarFamiliaNaLista(FamiliaComposite lista, String nome) {
		 // Se for o elemento raiz retorna ele
		 if (lista.nome.equalsIgnoreCase(nome)) {
			 return lista;
		 }else { 
			 // se n�o ent�o procura nos filhos
			 for (int i = 0; i < lista.filhos.size(); i++) {
				if (lista.filhos.get(i).nome.equalsIgnoreCase(nome)) {
					return lista.filhos.get(i);				
				}
			 }
		}
		return null;
	 }

	 public static void main(String[] args) {		 
		 // Inicializando vari�veis
		 FamiliaComponente familia = null;
		 String nome = "";
		 String nomeFilho = "";
		 String nomePai = "";
		 Scanner sc = new Scanner(System.in);
		 int comando = 0;
		 
		 imprimirMenu();
		 
		 // Loop que interage com o usu�rio
		 while(sc.hasNext()) {			
			comando = sc.nextInt();
			switch (comando) {
			case 0:
				imprimirMenu();
			case 1:
				mostrarArvoreAtual(familia);
				break;
			case 2:
				// Adiciona fam�lia
				System.out.println("Informe o nome da fam�lia e tecle ENTER:");
				nome = sc.next().trim();				
				if (familia == null) {
					System.out.println("Fam�lia " + nome + " adicionada como RAIZ da �rvore");
					familia = new FamiliaComposite(nome);
				}else {
					familia.add(new FamiliaComposite(nome));
					System.out.println("Fam�lia adicionada com sucesso");
				}				
				break;
			case 3:
				// Adiciona filho
				if (familia == null) {
					System.out.println("�rvore vazia! Informe uma fam�lia primeiro (op��o 2)");					
				}else {
					System.out.println("Informe o nome do filho e tecle ENTER:");
					nome = sc.next().trim();
					System.out.println("Informe o nome da familia que o filho pertence e tecle ENTER:");
					String familiaExistente = sc.next().trim();
					if (procurarFamiliaNaLista(( FamiliaComposite) familia, familiaExistente) != null) {
						procurarFamiliaNaLista(( FamiliaComposite) familia, familiaExistente).add(new Filhos(nome));
						System.out.println("Filho adicionado com sucesso");
					} else {
						System.out.println("Fam�lia n�o encontrada!");
					}
				}
				break;	
			case 4:
				// Adiciona familia em outra
				if (familia == null) {
					System.out.println("�rvore vazia! Informe uma fam�lia primeiro (op��o 2)");					
				}else {
					System.out.println("Informe o nome da fam�lia e tecle ENTER:");
					nome = sc.next().trim();
					System.out.println("Informe o nome da familia que ela pertence e tecle ENTER:");
					String familiaExistente = sc.next().trim();
					if (procurarFamiliaNaLista(( FamiliaComposite) familia, familiaExistente) != null) {
						procurarFamiliaNaLista(( FamiliaComposite) familia, familiaExistente).add(new FamiliaComposite(nome));
						System.out.println("Fam�lia adicionada com sucesso");
					} else {
						System.out.println("Fam�lia n�o encontrada!");
					}
				}
				break;
			case 5:
				// Remover elemento
				if (familia == null) {
					System.out.println("�rvore vazia! Informe uma fam�lia primeiro (op��o 2)");					
				}else {
					
					System.out.println("Informe o nome do elemento a ser removido e tecle ENTER:");
					nomeFilho = sc.next().trim();
					System.out.println("Informe o nome do PAI do elemento a ser removido e tecle ENTER:");
					nomePai = sc.next().trim();
					if (procurarFamiliaNaLista(( FamiliaComposite) familia, nomePai) != null) {
						if (procurarFamiliaNaLista(( FamiliaComposite) familia, nomePai).temFilhos()) {
							procurarFamiliaNaLista(( FamiliaComposite) familia, nomePai).rem(procurarFamiliaNaLista(( FamiliaComposite) familia, nomeFilho));
							System.out.println("Elemento removido com sucesso");
						}else {
							System.out.println("Esse elemento n�o tem filhos para serem removidos");
						}
					} else {
						System.out.println("Elemento n�o encontrado!");
					}
				
				}
				break;
			case 6:
				// Exporta �rvore
				if (familia != null) {
					List<Exportador> exportadores = new ArrayList<>();
					 exportadores.add(new AdaptadorExportadorJSON());
					 exportadores.add(new AdadpadorExportadorYAML());
					 exportadores.add(new AdadpadorExportadorTOML());
					 final FamiliaComponente familiaExport = familia;
					 exportadores.forEach(exportador -> exportador.exportar(familiaExport));					 
				}else {
					System.out.println("Nenhum objeto na �rvore!");
				}				 
				break;	
			case 7:
				fecharPrograma(sc);		
			default:
				System.out.println("Op��o inv�lida, tente novamente.");				
			}
			System.out.println("\n\n Digite um comando:");
		 }
	 }
}