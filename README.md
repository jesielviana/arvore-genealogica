# Trabalho da Disciplina de Arquitetura de Software Profissional.

## Como instalar e executar esse projeto

* Clone o repositrio

* Importe o projeto no eclipse: import -> General -> Projects from Folder or Archive

* Instale no library path do projeto as seguintes dependncias:

  gson-2.6.2.jar (https://repo1.maven.org/maven2/com/google/code/gson/gson/2.6.2/)
  
  snakeyaml-1.10.jar (http://central.maven.org/maven2/org/yaml/snakeyaml/1.10/)
  
  toml4j-0.7.2.jar (http://central.maven.org/maven2/com/moandjiezana/toml/toml4j/0.7.2/)

* Após instalados os jars das dependências, execute o programa via console no eclipse

* Aparecerá um menu com as opções suportadas para manipular a árvore genealógica.

## Equipe

* Rafael Marques - rbm@cesar.school
* Renan Oliveira - ron@cesar.school
* Joo Batista - jbfc@cesar.school
* Jesiel Viana - jvs@cesar.school
* Jackson Lima - jls@cesar.school
* Wilson Felipe - wfsl@cesar.school
* Francisco Franciosney - ffss@cesar.school
